package com.abdullah.turkcelldemo

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.abdullah.turkcelldemo.model.db.ProductDao
import com.abdullah.turkcelldemo.model.db.ProductDatabase
import com.abdullah.turkcelldemo.model.entity.Product
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseTest {
    private var dao: ProductDao? = null

    @Before
    fun createDb() {
        dao = ProductDatabase.getDatabase(InstrumentationRegistry.getInstrumentation().targetContext).productDao()
    }

    @Test
    fun shouldInsertAndGetProductListSuccessfully() {
        Thread(Runnable {
            val product = Product("1", "Apple", "123", "www.test.com", "test")
            dao!!.insertAllProducts(listOf(product))
            val productList = dao!!.getAllProducts().value
            assertEquals(product, productList!![0])
        }).start()

    }

    @Test
    fun shouldInsertAndGetProductListEmpty() {
        Thread(Runnable {
            dao!!.insertAllProducts(emptyList())
            val productList = dao!!.getAllProducts().value
            assert(productList!!.isEmpty())
        }).start()

    }

    @Test
    fun shouldUpdateProductSuccessfully() {
        val productUpdated = Product("1", "Apple", "123", "www.test.com", "test updated")
        Thread(Runnable {
            val productFirst = Product("1", "Apple", "123", "www.test.com", "test")
            dao!!.insertAllProducts(listOf(productFirst))
            dao!!.updateProduct(productUpdated)
            val productList = dao!!.getAllProducts().value
            assertNotEquals(productFirst.description, productList!![0].description)
        }).start()

    }

    @After
    fun after() {
        ProductDatabase.getDatabase(InstrumentationRegistry.getInstrumentation().targetContext).close()
    }
}