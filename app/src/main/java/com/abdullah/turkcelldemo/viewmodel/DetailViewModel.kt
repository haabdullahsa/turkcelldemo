package com.abdullah.turkcelldemo.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.model.repository.ProductRepository
import io.reactivex.disposables.CompositeDisposable

class DetailViewModel : ViewModel() {
    private var repository: ProductRepository = ProductRepository()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getProductDetailById(context: Context, id: String): LiveData<Product> {
        return repository.getProductById(context, id)
    }

    fun getProductDetailFromNetwork(context: Context, id: String) {
        return repository.getProductDetailAndPutInDB(context, compositeDisposable, id)
    }

    fun clearCompositeDisposable() {
        compositeDisposable.dispose()
    }
}