package com.abdullah.turkcelldemo.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.model.repository.ProductRepository
import io.reactivex.disposables.CompositeDisposable

class HomeViewModel : ViewModel() {
    private var repository: ProductRepository = ProductRepository()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getAllProductList(context: Context): LiveData<List<Product>> {
        return repository.getAllProducts(context)
    }

    fun getProductsFromNetwork(context: Context) {
        return repository.getProductsAndPutInDB(context, compositeDisposable)
    }

    fun clearCompositeDisposable() {
        compositeDisposable.dispose()
    }
}