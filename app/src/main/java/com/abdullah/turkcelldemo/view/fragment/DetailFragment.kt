package com.abdullah.turkcelldemo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.abdullah.turkcelldemo.R
import com.abdullah.turkcelldemo.util.Constants
import com.abdullah.turkcelldemo.util.Helper
import com.abdullah.turkcelldemo.viewmodel.DetailViewModel
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

class DetailFragment : Fragment() {

    lateinit var imageViewPhoto: ImageView
    lateinit var textViewTitle: TextView
    lateinit var textViewDescription: TextView

    private lateinit var detailViewModel: DetailViewModel

    lateinit var actionbar: ActionBar

    private var id: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        imageViewPhoto = view.findViewById(R.id.ivImage)
        textViewTitle = view.findViewById(R.id.tvName)
        textViewDescription = view.findViewById(R.id.tvDescription)

        id = arguments!!.getString(Constants.ARG_PRODUCT_ID)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        actionbar = (activity as AppCompatActivity).supportActionBar!!
        actionbar.setDisplayHomeAsUpEnabled(true)

        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)

        if (Helper.isNetworkConnected(requireContext())) {
            detailViewModel.getProductDetailFromNetwork(requireContext(), id!!)
        } else {
            Toast.makeText(requireContext(), getString(R.string.msg_offline_for_detail), Toast.LENGTH_SHORT).show()
        }

        detailViewModel.getProductDetailById(requireContext(), id!!).observe(this, Observer { product ->
            Helper.loadImage(
                requireContext(),
                imageViewPhoto,
                product.photoUrl,
                RequestOptions().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            textViewTitle.text = product.name
            textViewDescription.text = product.description
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        actionbar.setDisplayHomeAsUpEnabled(false)
        detailViewModel.clearCompositeDisposable()
    }
}