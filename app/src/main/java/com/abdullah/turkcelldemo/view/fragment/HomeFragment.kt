package com.abdullah.turkcelldemo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abdullah.turkcelldemo.R
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.util.Constants
import com.abdullah.turkcelldemo.util.Helper
import com.abdullah.turkcelldemo.view.adapter.ProductListAdapter
import com.abdullah.turkcelldemo.viewmodel.HomeViewModel

class HomeFragment : Fragment() {

    lateinit var recyclerView: RecyclerView

    lateinit var homeViewModel: HomeViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerView = view.findViewById(R.id.rv_list)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        if (Helper.isNetworkConnected(requireContext())) {
            homeViewModel.getProductsFromNetwork(requireContext())
        } else {
            Toast.makeText(requireContext(), getString(R.string.msg_offline_for_list), Toast.LENGTH_SHORT).show()
        }

        homeViewModel.getAllProductList(requireContext())
            .observe(this, Observer<List<Product>> { productList ->
                setUpCountryRecyclerView(productList)
            })

    }

    fun setUpCountryRecyclerView(products: List<Product>) {
        val productListAdapter =
            ProductListAdapter(products, itemClickListener = object : ProductListAdapter.ItemClickListener {
                override fun onItemClick(product: Product) {
                    val detailFragment = DetailFragment()
                    val bundle = Bundle()
                    bundle.putString(Constants.ARG_PRODUCT_ID, product.productId)
                    detailFragment.arguments = bundle
                    activity!!.supportFragmentManager.beginTransaction().add(R.id.container, detailFragment)
                        .addToBackStack(null)
                        .commit()
                }

            })
        recyclerView.adapter = productListAdapter
        recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        recyclerView.setHasFixedSize(true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        homeViewModel.clearCompositeDisposable()
    }
}