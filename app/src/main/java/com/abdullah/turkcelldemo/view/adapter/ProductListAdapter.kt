package com.abdullah.turkcelldemo.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abdullah.turkcelldemo.R
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.util.Helper
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_card_product.view.*

class ProductListAdapter(private val productList: List<Product>, private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card_product, parent, false))
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.initializeViews(productList[position])

        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(productList[holder.adapterPosition])
        }
    }


    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun initializeViews(product: Product) {
            val requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
            Helper.loadImage(itemView.context, itemView.iv_image, product.photoUrl, requestOptions)
            itemView.tv_name.text = product.name
            itemView.tv_price.text = product.price
        }

    }

    interface ItemClickListener {
        fun onItemClick(product: Product)
    }
}