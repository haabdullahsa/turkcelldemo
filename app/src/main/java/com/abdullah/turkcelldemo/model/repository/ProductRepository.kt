package com.abdullah.turkcelldemo.model.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.abdullah.turkcelldemo.model.db.ProductDatabase
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.model.network.NetworkProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class ProductRepository {

    val TAG = ProductRepository::class.java.name

    fun getAllProducts(context: Context): LiveData<List<Product>> {
        return ProductDatabase.getDatabase(context).productDao().getAllProducts()
    }

    fun getProductsAndPutInDB(context: Context, compositeDisposable: CompositeDisposable) {
        compositeDisposable.add(
            NetworkProvider.getInstance().getAllProducts().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                    { productList ->
                        Thread(Runnable {
                            ProductDatabase.getDatabase(context).productDao().deleteAllProducts()
                            ProductDatabase.getDatabase(context).productDao().insertAllProducts(productList)
                        }).start()
                    },
                    { error ->
                        Log.e(TAG, "Product List Error:", error)
                    })
        )
    }

    fun getProductById(context: Context, id: String): LiveData<Product> {
        return ProductDatabase.getDatabase(context).productDao().getProduct(id)
    }

    fun getProductDetailAndPutInDB(context: Context, compositeDisposable: CompositeDisposable, id: String) {
        compositeDisposable.add(
            NetworkProvider.getInstance().getProductDetail(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                    { product ->
                        Thread(Runnable {
                            ProductDatabase.getDatabase(context).productDao().updateProduct(product)
                        }).start()
                    },
                    { error ->
                        Log.e(TAG, "Product Detail Error:", error)
                    })
        )
    }
}