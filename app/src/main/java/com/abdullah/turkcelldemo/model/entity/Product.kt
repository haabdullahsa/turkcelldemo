package com.abdullah.turkcelldemo.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Products")
data class Product(
    @PrimaryKey
    @SerializedName("product_id")
    val productId: String = "",
    val name: String = "",
    val price: String = "",
    @SerializedName("image")
    val photoUrl: String = "",
    val description: String = ""
)