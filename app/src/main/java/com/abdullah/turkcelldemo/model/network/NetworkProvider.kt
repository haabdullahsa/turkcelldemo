package com.abdullah.turkcelldemo.model.network

import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.util.Constants
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkProvider {

    private var retrofit: Retrofit? = null
    private var restApi: RestApi? = null

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(Constants.END_POINT_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(OkHttpClient.Builder().build())
            .build()
        restApi = retrofit?.create(RestApi::class.java)
    }

    companion object {
        private var instance: NetworkProvider? = null

        public fun getInstance(): NetworkProvider {
            if (instance == null)
                instance = NetworkProvider()

            return instance as NetworkProvider
        }
    }

    fun getAllProducts(): Observable<List<Product>> {
        return restApi!!.getAllProducts().flatMapObservable { t -> Observable.just(t.products) }
    }

    fun getProductDetail(id: String): Single<Product> {
        return restApi!!.getProductDetailById(id)
    }
}