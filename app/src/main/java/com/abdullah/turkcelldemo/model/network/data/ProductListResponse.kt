package com.abdullah.turkcelldemo.model.network.data

import com.abdullah.turkcelldemo.model.entity.Product

data class ProductListResponse(
    var products: List<Product>
)