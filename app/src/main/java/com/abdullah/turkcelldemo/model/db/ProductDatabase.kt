package com.abdullah.turkcelldemo.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.util.Constants

@Database(entities = [(Product::class)], version = 1)
abstract class ProductDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    companion object {
        @Volatile
        private var instance: ProductDatabase? = null

        fun getDatabase(context: Context): ProductDatabase {
            val tempInstance = instance
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ProductDatabase::class.java,
                    Constants.DB_NAME
                ).build()
                this.instance = instance
                return instance
            }
        }
    }
}