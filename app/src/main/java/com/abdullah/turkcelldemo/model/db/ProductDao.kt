package com.abdullah.turkcelldemo.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.abdullah.turkcelldemo.model.entity.Product
import java.sql.RowId

@Dao
interface ProductDao {

    @Query("SELECT * FROM Products")
    fun getAllProducts(): LiveData<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProducts(products: List<Product>)

    @Query("DELETE FROM Products")
    fun deleteAllProducts()

    @Query("SELECT * FROM Products WHERE productId=:productId ")
    fun getProduct(productId: String): LiveData<Product>

    @Update
    fun updateProduct(product: Product)
}