package com.abdullah.turkcelldemo.model.network

import com.abdullah.turkcelldemo.model.entity.Product
import com.abdullah.turkcelldemo.model.network.data.ProductListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RestApi {
    @GET("cart/list")
    fun getAllProducts(): Single<ProductListResponse>

    @GET("cart/{productId}/detail")
    fun getProductDetailById(@Path("productId") productId: String): Single<Product>

}