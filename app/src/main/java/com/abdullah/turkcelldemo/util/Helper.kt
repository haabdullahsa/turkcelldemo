package com.abdullah.turkcelldemo.util

import android.content.Context
import android.net.ConnectivityManager
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions

class Helper {

    companion object {
        fun loadImage(context: Context, imageView: ImageView, url: String, options: RequestOptions) {
            GlideApp.with(context).applyDefaultRequestOptions(options)
                .load(url)
                .into(imageView)
        }

        fun isNetworkConnected(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        }
    }

}