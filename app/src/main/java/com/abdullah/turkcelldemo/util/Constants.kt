package com.abdullah.turkcelldemo.util

object Constants {
    var END_POINT_URL = "https://s3-eu-west-1.amazonaws.com/developer-application-test/"
    var DB_NAME = "product_database"

    //EXTRAS
    var ARG_PRODUCT_ID = "ARG_PRODUCT_ID"
}